# Basis B1 Data Importer

This command-line program will import a Basis B1 device's sensor data that are stored in CSV files into a SQLite database.

## User Guide

**Note:** You **must** have Java Runtime Environment (JRE) version 7 installed to run the application. You
can download the JRE for your operating system
[here](http://www.oracle.com/technetwork/java/javase/downloads/java-se-jre-7-download-432155.html).

1. Save your Basis B1 device's sensor data to CSV format using [this program](https://github.com/btroia/basis-data-export).

2. Download the data.db file, which can be found in the db directory.

3. Download and run the importer.jar file using the following required command-line arguments:

   `-data <dir>: directory that contains Basis B1 sensor data files`

   `-db <dir>: directory that contains the database file (data.db) to insert sensor data into`

## Example Usage

   `java -jar importer.jar -data "J:\My Documents\Basis B1" -db "J:\My Documents\Basis B1"`

**Note:** the directories don't have to be the same.

## Developer Guide

1. Install the following:
    * Java SDK version 7
    * Gradle version 2.2

2. Run `gradle build` to compile and assemble an executable JAR file.

3. Run `gradle eclipse` to build an Eclipse project.

4. Run `gradle check` to execute Checkstyle, PMD, and FindBugs.

## Notes

* I used my program to import a year's worth of data (**Dec 18, 2013 - Jan 21, 2015**).

> Number of records processed: 578,021

> Start time: 9:32:14 AM

> End time: 10:58:25 AM

> Running time: 01:26:10 (hours:minutes:seconds)

* My database file is 41 MB.

## Links

[Developer's Website](http://www.bjpeterdelacruz.com)
