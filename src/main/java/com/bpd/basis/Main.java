package com.bpd.basis;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

/**
 * This program will read in CSV files that contain Basis B1 sensor data (activities, metrics, and
 * sleep) and insert the sensor data into a SQLite database.
 * 
 * @author BJ Peter DeLaCruz
 */
public final class Main {

  private static final String DATA_OPTION = "data";
  private static final String DB_OPTION = "db";

  /** Do not instantiate this class. */
  private Main() {
  }

  /**
   * The main entry point of this application.
   * 
   * @param args Two arguments:
   * <ol>
   * <li><code>-data &lt;directory&gt;</code>: The directory that contains sensor data files.</li>
   * <li><code>-db &lt;directory&gt;</code>: The directory that contains the database file
   * (data.db).</li>
   * </ol>
   */
  public static void main(String... args) {
    Options options = new Options();

    String msg = "directory that contains Basis B1 sensor data files (required)";
    options.addOption(DATA_OPTION, true, msg);

    msg =
        "directory that contains the database file (data.db) to insert sensor data into (required)";
    options.addOption(DB_OPTION, true, msg);

    CommandLineParser parser = new BasicParser();
    CommandLine cmd;

    String dataDirectory, dbDirectory;
    try {
      cmd = parser.parse(options, args);
      if (cmd.hasOption(DATA_OPTION)) {
        dataDirectory = cmd.getOptionValue(DATA_OPTION);
      }
      else {
        throw new ParseException("");
      }
      if (cmd.hasOption(DB_OPTION)) {
        dbDirectory = cmd.getOptionValue(DB_OPTION);
      }
      else {
        throw new ParseException("");
      }
    }
    catch (ParseException pe) {
      HelpFormatter formatter = new HelpFormatter();
      formatter.printHelp("-data <directory> -db <directory>", options);
      return;
    }

    long startTime = System.currentTimeMillis();

    System.out.println("Processing activities data...");
    Importer importer = new Importer(dbDirectory);
    importer.importData(dataDirectory, BasisType.ACTIVITIES);
    long numRecords = importer.getNumRecords();

    System.out.println("\nProcessing metrics data...");
    importer = new Importer(dataDirectory);
    importer.importData(dataDirectory, BasisType.METRICS);
    numRecords += importer.getNumRecords();

    System.out.println("\nProcessing sleep data...");
    importer = new Importer(dataDirectory);
    importer.importData(dataDirectory, BasisType.SLEEP);
    numRecords += importer.getNumRecords();

    long endTime = System.currentTimeMillis();
    long millis = endTime - startTime;

    final SimpleDateFormat formatter = new SimpleDateFormat("h:mm:ss a");
    final String startTimeString = formatter.format(startTime);
    final String endTimeString = formatter.format(endTime);

    long second = (millis / 1000) % 60;
    long minute = (millis / (1000 * 60)) % 60;
    long hour = (millis / (1000 * 60 * 60)) % 24;

    String time = String.format("%02d:%02d:%02d", hour, minute, second);

    System.out.println("\nNumber of records processed: "
        + NumberFormat.getInstance().format(numRecords) + "\n");
    System.out.println("Start time: " + startTimeString);
    System.out.println("End time: " + endTimeString + "\n");
    System.out.println("Running time: " + time + " (hours:minutes:seconds)");
  }

}
