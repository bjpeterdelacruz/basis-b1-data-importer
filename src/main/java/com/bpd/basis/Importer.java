package com.bpd.basis;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.Charset;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;

/**
 * This class contains methods that will import Basis B1 sensor data into a SQLite database.
 * 
 * @author BJ Peter DeLaCruz
 */
public class Importer {

  private static final String DATA_FILE = "data.db";

  private static final String INSERT_ACTIVITIES =
      "INSERT INTO activities (starttime,starttimemillis,starttimeiso,starttimetimezone,"
          + "starttimeoffset,endtime,endtimemillis,endtimeiso,endtimetimezone,endtimeoffset,"
          + "type,actualseconds,steps,calories,minutes,heartrateavg,heartratemin,"
          + "heartratemax,state,version,id)"
          + " VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

  private static final String INSERT_METRICS =
      "INSERT INTO metrics (timestamp,timestampmillis,heartrate,steps,calories,gsr,"
          + "skintemp,airtemp) VALUES(?,?,?,?,?,?,?,?)";

  private static final String INSERT_SLEEP =
      "INSERT INTO sleep (starttime,starttimemillis,starttimeiso,starttimetimezone,"
          + "starttimeoffset,endtime,endtimemillis,endtimeiso,endtimetimezone,endtimeoffset,"
          + "lightmins,deepmins,remmins,interruptionmins,unknownmins,interruptions,tossturns,"
          + "type,actualseconds,calories,heartrateavg,heartratemin,heartratemax,"
          + "state,version,id)"
          + " VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

  private static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";

  private final List<Path> data;
  private final Connection connection;
  private BasisType type;
  private long numRecords = 0;

  /**
   * Creates a new Importer instance.
   * 
   * @param databaseDirectory The directory that contains the database file (data.db).
   */
  public Importer(String databaseDirectory) {
    if (databaseDirectory == null || databaseDirectory.isEmpty()) {
      throw new RuntimeException("databaseDirectory is null or empty");
    }

    data = new ArrayList<Path>();

    try {
      String connectionString = "jdbc:sqlite:" + databaseDirectory + File.separator + DATA_FILE;
      Class.forName("org.sqlite.JDBC");
      connection = DriverManager.getConnection(connectionString);
      String enableForeignKeyConstraints = "PRAGMA foreign_keys = ON";
      PreparedStatement stmt =
          connection.prepareStatement(enableForeignKeyConstraints, Statement.NO_GENERATED_KEYS);
      stmt.execute();
      stmt.close();
    }
    catch (Exception e) {
      throw new RuntimeException("Unable to connect to database.", e);
    }
  }

  /**
   * Imports all the sensor data of the given type located in the given directory into the SQLite
   * database.
   * 
   * @param startingDirectory The directory that contains the sensor data to import.
   * @param type The type of sensor data to import (activities, metrics, or sleep).
   */
  public void importData(String startingDirectory, BasisType type) {
    if (startingDirectory == null || startingDirectory.isEmpty()) {
      throw new RuntimeException("startingDirectory is null or empty");
    }
    if (type == null) {
      throw new RuntimeException("type is null");
    }

    this.type = type;

    Path startingPath = new File(startingDirectory).toPath();
    try {
      Files.walkFileTree(startingPath, new BasisFileVisitor(type));
    }
    catch (IOException e) {
      String msg = "Error occurred while trying to get files in " + startingDirectory;
      throw new RuntimeException(msg, e);
    }

    for (Path file : data) {
      System.out.print("Processing " + file + "... ");
      try {
        importData(file);
      }
      catch (Exception e) {
        e.printStackTrace();
      }
      System.out.println("Finished.");
    }

    try {
      connection.close();
    }
    catch (SQLException e) {
      throw new RuntimeException("Couldn't close SQL connection.", e);
    }
    System.out.println("Import of " + type.getType() + " data finished!");
  }

  /**
   * Imports the sensor data in the given file into the SQLite database.
   * 
   * @param file The file that contains the sensor data of the given type to import.
   * @throws Exception If there are problems trying to import the sensor data into the database.
   */
  private void importData(Path file) throws Exception {
    if (file == null || !file.toFile().exists()) {
      throw new RuntimeException("file is null or does not exist.");
    }

    Reader in = null;
    try {
      in = new InputStreamReader(new FileInputStream(file.toFile()), Charset.forName("UTF-8"));
      Iterable<CSVRecord> records = CSVFormat.EXCEL.withHeader().parse(in);
      switch (type) {
      case ACTIVITIES: {
        for (CSVRecord record : records) {
          String starttime = record.get("start time");
          String starttimeiso = record.get("start time ISO");
          String starttimetimezone = record.get("start time timezone");
          String starttimeoffset = record.get("start time offset");
          String endtime = record.get("end time");
          String endtimeiso = record.get("end time ISO");
          String endtimetimezone = record.get("end time timezone");
          String endtimeoffset = record.get("end time offset");
          String typeStr = record.get("type");
          String actualseconds = record.get("actual seconds");
          String steps = record.get("steps");
          String calories = record.get("calories");
          String minutes = record.get("minutes");
          String heartrateavg = record.get("heart rate avg");
          String heartratemin = record.get("heart rate min");
          String heartratemax = record.get("heart rate max");
          String state = record.get("state");
          String version = record.get("version");
          String id = record.get("id");

          PreparedStatement statement = null;
          try {
            statement = connection.prepareStatement(INSERT_ACTIVITIES);
            statement.setString(1, starttime);
            statement.setLong(2, convertDatetimeStringToMillis(starttime));
            statement.setString(3, starttimeiso);
            statement.setString(4, starttimetimezone);
            statement.setInt(5, Integer.parseInt(starttimeoffset));
            statement.setString(6, endtime);
            statement.setLong(7, convertDatetimeStringToMillis(endtime));
            statement.setString(8, endtimeiso);
            statement.setString(9, endtimetimezone);
            statement.setInt(10, Integer.parseInt(endtimeoffset));
            statement.setString(11, typeStr);
            statement.setInt(12, Integer.parseInt(actualseconds));
            if (!steps.isEmpty()) {
              statement.setInt(13, Integer.parseInt(steps));
            }
            statement.setDouble(14, Double.parseDouble(calories));
            statement.setDouble(15, Double.parseDouble(minutes));
            if (!heartrateavg.isEmpty()) {
              statement.setDouble(16, Double.parseDouble(heartrateavg));
            }
            if (!heartratemin.isEmpty()) {
              statement.setDouble(17, Double.parseDouble(heartratemin));
            }
            if (!heartratemax.isEmpty()) {
              statement.setDouble(18, Double.parseDouble(heartratemax));
            }
            statement.setString(19, state);
            statement.setInt(20, Integer.parseInt(version));
            statement.setString(21, id);

            statement.execute();
          }
          finally {
            if (statement != null) {
              statement.close();
            }
          }
          numRecords++;
        }
        break;
      }
      case METRICS: {
        for (CSVRecord record : records) {
          String timestamp = record.get("timestamp");
          String heartrate = record.get("heartrate");
          String steps = record.get("steps");
          String calories = record.get("calories");
          String gsr = record.get("gsr");
          String skintemp = record.get("skintemp");
          String airtemp = record.get("airtemp");

          PreparedStatement statement = null;
          try {
            statement = connection.prepareStatement(INSERT_METRICS);
            statement.setString(1, timestamp);
            statement.setLong(2, convertDatetimeStringToMillis(timestamp));
            if (!heartrate.isEmpty()) {
              statement.setInt(3, Integer.parseInt(heartrate));
            }
            if (!steps.isEmpty()) {
              statement.setInt(4, Integer.parseInt(steps));
            }
            if (!calories.isEmpty()) {
              statement.setDouble(5, Double.parseDouble(calories));
            }
            if (!gsr.isEmpty()) {
              statement.setDouble(6, Double.parseDouble(gsr));
            }
            if (!skintemp.isEmpty()) {
              statement.setDouble(7, Double.parseDouble(skintemp));
            }
            if (!airtemp.isEmpty()) {
              statement.setDouble(8, Double.parseDouble(airtemp));
            }

            statement.execute();
          }
          finally {
            if (statement != null) {
              statement.close();
            }
          }
          numRecords++;
        }
        break;
      }
      case SLEEP:
        importSleepData(records);
        break;
      default:
        throw new RuntimeException(type.getType() + " is not supported.");
      }
    }
    finally {
      if (in != null) {
        in.close();
      }
    }
  }

  /**
   * Imports sleep sensor data into the SQLite database.
   * 
   * @param records The sleep sensor data to import.
   * @throws Exception If there are problems trying to import the sleep sensor data into the
   * database.
   */
  private void importSleepData(Iterable<CSVRecord> records) throws Exception {
    if (records == null) {
      throw new RuntimeException("record is null");
    }

    for (CSVRecord record : records) {
      String starttime = record.get("start time");
      String starttimeiso = record.get("start time ISO");
      String starttimetimezone = record.get("start time timezone");
      String starttimeoffset = record.get("start time offset");
      String endtime = record.get("end time");
      String endtimeiso = record.get("end time ISO");
      String endtimetimezone = record.get("end time timezone");
      String endtimeoffset = record.get("end time offset");
      String lightmins = record.get("light mins");
      String deepmins = record.get("deep mins");
      String remmins = record.get("rem mins");
      String interruptionmins = record.get("interruption mins");
      String unknownmins = record.get("unknown mins");
      String interruptions = record.get("interruptions");
      String tossturns = record.get("toss turns");
      String typeStr = record.get("type");
      String actualseconds = record.get("actual seconds");
      String calories = record.get("calories");
      String heartrateavg = record.get("heart rate avg");
      String heartratemin = record.get("heart rate min");
      String heartratemax = record.get("heart rate max");
      String state = record.get("state");
      String version = record.get("version");
      String id = record.get("id");

      PreparedStatement statement = null;
      try {
        statement = connection.prepareStatement(INSERT_SLEEP);
        statement.setString(1, starttime);
        statement.setLong(2, convertDatetimeStringToMillis(starttime));
        statement.setString(3, starttimeiso);
        statement.setString(4, starttimetimezone);
        statement.setInt(5, Integer.parseInt(starttimeoffset));
        statement.setString(6, endtime);
        statement.setLong(7, convertDatetimeStringToMillis(endtime));
        statement.setString(8, endtimeiso);
        statement.setString(9, endtimetimezone);
        statement.setInt(10, Integer.parseInt(endtimeoffset));
        statement.setInt(11, Integer.parseInt(lightmins));
        statement.setInt(12, Integer.parseInt(deepmins));
        statement.setInt(13, Integer.parseInt(remmins));
        statement.setInt(14, Integer.parseInt(interruptionmins));
        statement.setInt(15, Integer.parseInt(unknownmins));
        statement.setInt(16, Integer.parseInt(interruptions));
        statement.setInt(17, Integer.parseInt(tossturns));
        statement.setString(18, typeStr);
        statement.setInt(19, Integer.parseInt(actualseconds));
        statement.setDouble(20, Double.parseDouble(calories));
        if (!heartrateavg.isEmpty()) {
          statement.setDouble(21, Double.parseDouble(heartrateavg));
        }
        if (!heartratemin.isEmpty()) {
          statement.setDouble(22, Double.parseDouble(heartratemin));
        }
        if (!heartratemax.isEmpty()) {
          statement.setDouble(23, Double.parseDouble(heartratemax));
        }
        statement.setString(24, state);
        statement.setInt(25, Integer.parseInt(version));
        statement.setString(26, id);

        statement.execute();
      }
      finally {
        if (statement != null) {
          statement.close();
        }
      }
      numRecords++;
    }
  }

  /** @return The total number of records that were imported into the database. */
  public long getNumRecords() {
    return numRecords;
  }

  /**
   * A FileVisitor that will add files that contain sensor data of a given type to a list.
   * 
   * @author BJ Peter DeLaCruz
   */
  private class BasisFileVisitor extends SimpleFileVisitor<Path> {

    private BasisType type;

    /**
     * Creates a new BasicFileVisitor instance.
     * 
     * @param type The type of sensor data.
     */
    public BasisFileVisitor(BasisType type) {
      if (type == null) {
        throw new RuntimeException("type is null");
      }
      this.type = type;
    }

    /** {@inheritDoc} */
    @Override
    public FileVisitResult visitFile(Path file, BasicFileAttributes attr) {
      if (attr.isRegularFile() && file.getFileName().toString().endsWith(type.getType() + ".csv")) {
        data.add(file);
      }
      return FileVisitResult.CONTINUE;
    }
  }

  /**
   * Converts a date-time string in the format <code>yyyy-MM-dd HH:mm:ss</code> to milliseconds.
   * 
   * @param datetime The string to convert, e.g. "2013-12-19 08:17:14".
   * @return The milliseconds.
   * @throws Exception If there are problems converting the string to milliseconds.
   */
  private static long convertDatetimeStringToMillis(String datetime) throws Exception {
    final SimpleDateFormat formatter = new SimpleDateFormat(DATE_FORMAT);
    return formatter.parse(datetime).getTime();
  }

}
