/**
 * This package contains:
 * 
 * <ol>
 * <li>An enum to represent the different Basis B1 sensor data types.</li>
 * <li>A class that contains methods to import the sensor data into a SQLite database.</li>
 * <li>The main class.</li>
 * </ol>
 */
package com.bpd.basis;