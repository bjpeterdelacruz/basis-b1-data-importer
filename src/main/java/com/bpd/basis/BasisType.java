package com.bpd.basis;

/**
 * This enum represents the different types of Basis B1 sensor data.
 * 
 * @author BJ Peter DeLaCruz
 */
public enum BasisType {

  /** Activities data. */
  ACTIVITIES("activities"),
  /** Metrics data. */
  METRICS("metrics"),
  /** Sleep data. */
  SLEEP("sleep");

  private final String type;

  /**
   * Creates a new BasisType enum.
   * 
   * @param type The type of sensor data.
   */
  BasisType(String type) {
    this.type = type;
  }

  /** @return The type of sensor data. */
  public String getType() {
    return type;
  }

}
